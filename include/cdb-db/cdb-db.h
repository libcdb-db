#ifndef CDB_DB_CDBDB_H_
#define CDBDB_CDBDB_H_

#include <vector>
#include <gflags/gflags.h>

DECLARE_string(db_host);
DECLARE_string(db_name);
DECLARE_int32(db_port);
DECLARE_string(db_user);
DECLARE_string(db_passwd);
// =============================================================================
// Forward declaration
// =============================================================================
namespace mysqlpp {
	class Connection;
}
namespace cdb_db{
	class CdbProfile;
}
// =============================================================================
namespace cdb_db{
// =============================================================================
typedef std::vector<CdbProfile*> CdbProfilesList;
// =============================================================================
class CdbDb{
public:
// -----------------------------------------------------------------------------
	CdbDb();
	~CdbDb();
// -----------------------------------------------------------------------------
	bool connect();
	CdbProfilesList* profiles();
// -----------------------------------------------------------------------------
private:
	CdbProfilesList* profiles_;
	mysqlpp::Connection* connection_;

};
// =============================================================================
}
// =============================================================================
#endif // CDB_DB_CDBDB_H_
