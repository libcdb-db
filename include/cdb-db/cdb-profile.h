/*
 * cdb-profile.h
 *
 *  Created on: Oct 24, 2008
 *      Author: Alexander MAZUROV (alexander.mazurov@gmail.com)
 */

#ifndef CDB_DB_CDBPROFILE_H_
#define CDB_DB_CDBPROFILE_H_

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <time.h>

namespace cdb_db{
// =============================================================================
namespace{
  template<typename T> inline bool fromString(const std::string& value,T& result)
  {
      std::stringstream input(value);
      if ( !(input >> result)) return false;
      return true;
  }
}
// =============================================================================
class CdbProfile {
public:
  const std::string& obj() const { return obj_;}
  const time_t& mtime() const { return mtime_;}
  template <typename T> bool property(const std::string& path);
protected:
  virtual bool property(const std::string& path, const std::string&value)  = 0;
  virtual bool property(const std::string& path, const std::vector<std::string>& values)  = 0;
  CdbProfile(const std::string& obj,const time_t& mtime):obj_(obj),mtime_(mtime){}
  //void set_obj(const std::string& obj){obj_ = obj;}
  //void set_mtime(){mtime_ = mtime;}
private:
   std::string obj_;
  time_t mtime_;

};
// =============================================================================
}
// =============================================================================
#endif /* CDB_DB_CDBPROFILE_H_ */
