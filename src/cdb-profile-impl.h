/*
 * cdb-profile-impl.h
 *
 *  Created on: Oct 24, 2008
 *      Author: Alexander MAZUROV (alexander.mazurov@gmail.com)
 */

#ifndef CDB_DB_CDBPROFILEIMPL_H_
#define CDB_DB_CDBPROFILEIMPL_H_
// =============================================================================
#include "cdb-db/cdb-profile.h"
// =============================================================================
namespace mysqlpp
{
    class Connection;
}
// =============================================================================
namespace cdb_db{
// =============================================================================
class CdbProfileImpl : public CdbProfile
{
public:
  CdbProfileImpl(mysqlpp::Connection* connection,
      const std::string& obj, const time_t& mtime)
  :CdbProfile(obj,mtime),connection_(connection){}
private:
  bool property(const std::string& path, const std::string&value)
  { return true;}
  bool property(const std::string& path, const std::vector<std::string>& values)
  { return true;}

private:
  mysqlpp::Connection* connection_;
};
// =============================================================================
}

#endif /* CDB_DB_CDBPROFILEIMPL_H_ */
