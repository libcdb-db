#include "cdb-db/cdb-db.h"

#include <mysql++.h>
#include <glog/logging.h>

#include "cdb-profile-impl.h"
// =============================================================================
DEFINE_string(db_host,"localhost","Host of mysql");
DEFINE_int32(db_port,3306,"Port of cdb's database");
DEFINE_string(db_name,"cdb","Database name where cdb information stored");
DEFINE_string(db_user,"root","User name for access database");
DEFINE_string(db_passwd,"","User's password");
// =============================================================================
using namespace std;
using namespace mysqlpp;
// =============================================================================
namespace cdb_db{
// =============================================================================
CdbDb::CdbDb(){
	connection_ = new Connection(false);
	profiles_ = NULL;
}
// =============================================================================
CdbDb::~CdbDb(){
	connection_ = new Connection(false);
	if (NULL != profiles_){
	  for(CdbProfilesList::const_iterator current = profiles_->begin();
        current != profiles_->end();current++) {
	    delete *current;
	  }
	  delete profiles_;
	}
}
// =============================================================================
bool CdbDb::connect()
{
	VLOG(1)<<"Connect to database:"
		<< "db_name=" << FLAGS_db_name
		<< ", host=" << FLAGS_db_host
		<< ", user=" << FLAGS_db_user
		<< ", password=" << FLAGS_db_passwd
		<< ", port=" << FLAGS_db_port;
	if (!connection_->connect(FLAGS_db_name.c_str(),FLAGS_db_host.c_str(),
			FLAGS_db_user.c_str(), FLAGS_db_passwd.c_str(), FLAGS_db_port)){
		LOG(ERROR)<<connection_->error();
		return false;
	}
	VLOG(1)<<"...connected";
	return true;
}
// =============================================================================
CdbProfilesList* CdbDb::profiles()
{
	if (NULL == profiles_){
		profiles_ = new CdbProfilesList();
		Query query = connection_->query(
            "select obj,mtime  FROM objtimes ORDER BY mtime DESC"
		);
		StoreQueryResult res = query.store();
		if (!res){
			LOG(FATAL) << "Failed to get profiles. " << query.error();
		}
		for (size_t i = 0; i < res.num_rows(); ++i) {
		  DateTime datetime(res[i]["mtime"]);
		  profiles_->push_back(
		      new CdbProfileImpl(connection_,
		          string(res[i]["obj"]),
		          time_t(datetime)));
		}
	}
	return profiles_;
}
// =============================================================================
}
// =============================================================================
